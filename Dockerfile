FROM python:3.8

RUN python3 -m pip install --upgrade 'python-telegram-bot<20'

ENV REQUEST_URL=https://bad-dragon.com/api/inventory-toys?type[]=ready_made&type[]=flop&price[min]=0&price[max]=300&category=insertable&sizes[]=8&sizes[]=3&sort[field]=price&&sort[direction]=asc&page=1&limit=60

ADD main.py /
ENTRYPOINT [ "python3", "/main.py" ]

